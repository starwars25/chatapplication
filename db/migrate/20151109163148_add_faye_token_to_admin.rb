class AddFayeTokenToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :faye_token, :string
  end
end
