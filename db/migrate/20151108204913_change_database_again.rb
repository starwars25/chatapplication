class ChangeDatabaseAgain < ActiveRecord::Migration
  def change
    remove_column :admins, :password
    remove_column :admins, :authenticationDigest

    add_column :admins, :password_digest, :string
    add_column :admins, :authentication_digest, :string
  end
end
