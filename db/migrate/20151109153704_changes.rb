class Changes < ActiveRecord::Migration
  def change
    remove_column :admins, :email
    add_column :admins, :login, :string
    add_column :admins, :name, :string
    add_column :admins, :super_user, :boolean, default: false
  end
end
