class ChangeDatabase < ActiveRecord::Migration
  def change
    remove_column :admins, :password_digest
    remove_column :admins, :token
    add_column :admins, :email, :string
    add_column :admins, :authenticationDigest, :string
  end
end
