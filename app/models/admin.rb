class Admin < ActiveRecord::Base
  attr_accessor :token

  def create_password(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def authenticate
    self.token = SecureRandom.uuid
    auth_digest = BCrypt::Password.create(self.token)

    update_attribute :authentication_digest, auth_digest

  end

  def generate_faye_token
    update_attribute :faye_token, SecureRandom.uuid
  end

  def authorize?(token)
    BCrypt::Password.new(self.authentication_digest) == token
  end


end
