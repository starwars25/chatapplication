class AdminController < ApplicationController
  before_action :check_user, only: [:logout, :change_password]

  def login

    admin = Admin.where("login = '#{params[:login]}'").first
    if admin
      password = BCrypt::Password.new(admin.password_digest)
      if password == params[:password]
        admin.authenticate
        admin.generate_faye_token
        render json: {result: 'success', token: admin.token, id: admin.id, name: admin.name, faye_token: admin.faye_token}
      else
        render json: {result: 'error', description: 'wrong password'}
      end
    else
      render json: {result: 'error', desription: 'no admin with such email'}
    end
  end

  def logout
    user = Admin.where("id = #{request.headers['user-id']}").first
    user.authentication_digest = nil
    user.faye_token = nil
    user.save
    render json: {result: 'success'}


  end

  def change_password
    if params[:password] != params[:password_confirmation]
      render json: {result: 'failure', description: 'passwords do not match'}
    end
    if params[:password].length < 4
      render json: {result: 'failure', decription: 'password is invalid'}
    end
    user = Admin.where("id = #{request.headers['user-id']}").first

    user.authentication_digest = nil
    user.faye_token = nil
    user.password_digest = BCrypt::Password.create(params[:password])
    user.save
    byebug
    render json: {result: 'success'}
  end

  private
  def check_user
    render json: {result: 'failure', description: 'No authorized'} unless current_user
  end



end
