class MessagesController < ApplicationController
  include AdminHelper
  before_action :check_admin, only: [:update, :destroy, :create]

  def index
    render json: Message.all
  end

  def show
    message = Message.find_by(id: params[:id])
    if message
      render json: message
    else
      render json: {result: 'failure'}
    end
  end

  def create
    message = Message.new(text: params[:message][:text])
    user = Admin.find_by(params[:sender_id])
    message.author = user.name
    if message.save
      render json: {result: 'success'}
      Admin.all.each do |u|
        broadcast("/#{u.faye_token}", {message: 'refresh', code: '200', id: message.id})
      end

    else
      render json: {result: 'failure'}
    end

  end

  def update
    message = Message.find_by(id: params[:id])
    if message
      message.author = params[:message][:author]
      message.text = params[:message][:text]
      if message.save
        render json: {result: 'success'}
        Admin.all.each do |u|
          broadcast("/#{u.faye_token}", {message: 'refresh_all', code: '200', id: message.id})
        end
      else
        render json: {result: 'failure', description: 'Invalid data'}
      end
    else
      render json: {result: 'failure', description: 'No such message'}
    end
  end

  def destroy
    message = Message.find_by(id: params[:id])
    if message
      message.destroy
      render json: {result: 'success'}
      Admin.all.each do |u|
        broadcast("/#{u.faye_token}", {message: 'refresh_all', code: '200', id: message.id})
      end
    else
      render json: {result: 'failure', description: 'No such message'}
    end
  end

  private
  def check_admin
    if current_user == nil
      render json: {result: 'failure', description: 'Not logged in'}
    elsif !current_user.super_user
      render json: {result: 'failure', description: 'Not admin'}
    end
  end
end
