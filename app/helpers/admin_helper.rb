# noinspection ALL
module AdminHelper
  def current_user
    sql = "id = #{request.headers['user-id']}"
    user = Admin.where(sql).first
    if user
      if user.authorize? request.headers['auth-token']
        return user
      else
        return nil
      end
    else
      return nil
    end
  end


end
