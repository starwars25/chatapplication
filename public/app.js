var chat = angular.module('chat', ['ngCookies', 'ngRoute']);

chat.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.when('/chat', {
        templateUrl: 'partials/main.html',
        controller: 'main'
    }).when('/admin', {
        templateUrl: 'partials/admin.html',
        controller: 'admin'
    }).when('/changepassword', {
        templateUrl: 'partials/password.html',
        controller: 'change_password'
    }).otherwise({
        redirectTo: '/chat'
    });
}]);

chat.controller('admin', ['$scope', '$http', '$cookies', function ($scope, $http, $cookies) {
    function checkCookies() {
        $scope.adminId = $cookies.get('id');
        $scope.auth = $cookies.get('auth');
        $scope.name = $cookies.get('username');
        $scope.fayeToken = $cookies.get('fayeToken');
        $scope.loggedIn = !!($scope.adminId != undefined && $scope.auth != undefined && $scope.name != undefined && $scope.fayeToken != undefined);
        if ($scope.loggedIn) {
            getMessages();
        }
    }

    checkCookies();

    function getMessages() {
        var request = {
            method: 'GET',
            url: '/messages'


        };

        $http(request).then(function successCallback(response) {
            $scope.messages = response.data;
        }, function errorCallback(response) {
            alert('Error while pulling messages');
        });
    }

    $scope.delete = function (index) {
        var request = {
            method: 'DELETE',
            url: '/messages/' + $scope.messages[index].id,
            headers: {
                'user-id': $scope.adminId,
                'auth-token': $scope.auth
            }
        };
        $http(request).then(function s(r) {
            console.log(r.data);
            if (r.data.result === 'success') {
                $scope.messages.splice(index, 1);
            }
        }, function f(r) {

        });
    };

    $scope.edit = function (index) {
        $scope.editing = true;
        $scope.editedMessage = $scope.messages[index];
        $scope.editedId = $scope.editedMessage.id;
        $scope.editedAuthor = $scope.editedMessage.author;
        $scope.editedText = $scope.editedMessage.text;

    };

    $scope.update = function () {
        //console.log('updating');
        var request = {
            method: 'PUT',
            url: '/messages/' + $scope.editedId,
            headers: {
                'user-id': $scope.adminId,
                'auth-token': $scope.auth,
                'content-type': 'application/json'
            },
            data: {
                message: {
                    author: $scope.editedAuthor,
                    text: $scope.editedText
                }
            }
        };
        $http(request).then(function success(response) {
            console.log(response.data);
            if (response.data.result == 'success') {
                $scope.editedMessage.text = $scope.editedText;
                $scope.editedMessage.author = $scope.editedAuthor;

                $scope.editedMessage = undefined;
                $scope.editing = false;


            }
        }, function failure(response) {

        });

    };


    $scope.logIn = function () {
        var r = {
            method: 'POST',
            url: '/login',
            headers: {
                'content-type': 'application/json'
            },
            data: {
                login: $scope.login,
                password: $scope.password
            }
        };
        $http(r).then(function s(resp) {
            if (resp.data.result === 'success') {
                var expires = new Date();
                expires.setSeconds(expires.getSeconds() + 60 * 60 * 24 * 14);
                $cookies.put('id', resp.data.id, {expires: expires});
                $cookies.put('auth', resp.data.token, {expires: expires});
                $cookies.put('username', resp.data.name, {expires: expires});
                $cookies.put('fayeToken', resp.data.faye_token, {expires: expires});
                $scope.adminId = resp.data.id;
                $scope.auth = resp.data.token;
                $scope.loggedIn = true;
                getMessages();
            } else {
                alert('error');
            }

        }, function f(resp) {
            console.log(resp.data);
        });

    };

    $scope.logOut = function () {
        var request = {
            method: 'POST',
            url: '/logout',
            headers: {
                'user-id': $scope.adminId,
                'auth-token': $scope.auth
            }
        };
        $http(request).then(function s(r) {
            console.log(r.data);
        }, function f(r) {

        });
        $cookies.remove('id');
        $cookies.remove('auth');
        $cookies.remove('username');
        $cookies.remove('fayeToken');
        $scope.adminId = undefined;
        $scope.auth = undefined;
        $scope.loggedIn = false;
        $scope.messages = [];
    }

}]);

chat.controller('main', ['$scope', '$timeout', '$cookies', '$http', function ($scope, $timeout, $cookies, $http) {

    checkCookies();


    function checkCookies() {
        $scope.adminId = $cookies.get('id');
        $scope.auth = $cookies.get('auth');
        $scope.name = $cookies.get('username');
        $scope.fayeToken = $cookies.get('fayeToken');
        $scope.loggedIn = !!($scope.adminId != undefined && $scope.auth != undefined && $scope.name != undefined);
        if ($scope.loggedIn) {
            getMessages();
            subscribe();
        }
    }

    function subscribe() {
        var client = new Faye.Client('http://localhost:9292/faye');
        client.subscribe('/' + $scope.fayeToken, function (message) {
            if (message.message === 'refresh_all') {
                getMessages();
            } else {
                pullMessage(message.id);

            }
        });
    }

    $scope.logOut = function () {
        var request = {
            method: 'POST',
            url: '/logout',
            headers: {
                'user-id': $scope.adminId,
                'auth-token': $scope.auth
            }
        };
        $http(request).then(function s(r) {
            console.log(r.data);
        }, function f(r) {

        });
        $cookies.remove('id');
        $cookies.remove('auth');
        $cookies.remove('username');
        $cookies.remove('fayeToken');
        $scope.adminId = undefined;
        $scope.auth = undefined;
        $scope.name = undefined;
        $scope.fayeToken = undefined;
        $scope.loggedIn = false;
        $scope.messages = [];
    };

    $scope.logIn = function () {
        var r = {
            method: 'POST',
            url: '/login',
            headers: {
                'content-type': 'application/json'
            },
            data: {
                login: $scope.login,
                password: $scope.password
            }
        };
        $http(r).then(function s(resp) {
            if (resp.data.result === 'success') {
                var expires = new Date();
                expires.setSeconds(expires.getSeconds() + 60 * 60 * 24 * 14);
                $cookies.put('id', resp.data.id, {expires: expires});
                $cookies.put('auth', resp.data.token, {expires: expires});
                $cookies.put('username', resp.data.name, {expires: expires});
                $cookies.put('fayeToken', resp.data.faye_token, {expires: expires});
                $scope.adminId = resp.data.id;
                $scope.auth = resp.data.token;
                $scope.name = resp.data.name;
                $scope.fayeToken = resp.data.faye_token;
                subscribe();
                $scope.loggedIn = true;
                getMessages();
            } else {
                alert('error');
            }

        }, function f(resp) {
            console.log(resp.data);
        });

    };


    function getMessages() {
        var request = {
            method: 'GET',
            url: '/messages'


        };

        $http(request).then(function successCallback(response) {
            $scope.messages = response.data;
            scroll();
        }, function errorCallback(response) {

        });
    }

    function pullMessage(id) {
        var req = {
            method: 'GET',
            url: '/messages/' + id
        };
        $http(req).then(function s(response) {
            $scope.messages.push(response.data);
            scroll();
        }, function f(response) {
            alert('Error while updating');
        });
    }


    $scope.determinePosition = function (number) {
        if (number % 2 == 0) {
            return 'even';
        } else {
            return 'odd';
        }
    };

    $scope.sendMessage = function () {
        if ($scope.messageText != '' && $scope.pickedName != '') {
            var request = {
                method: 'POST',
                url: '/messages',
                data: {
                    message: {
                        author: $scope.pickedName,
                        text: $scope.messageText
                    },

                    'sender_id': $scope.adminId
                },
                headers: {
                    'content-type': 'application/json',
                    'user-id': $scope.adminId,
                    'auth-token': $scope.auth
                }


            };
            $http(request).then(function success(response) {

            }, function failure(response) {

            });
        } else {
            alert('error');
        }
    };

    $scope.keyHit = function (event, element) {
        if (event.keyCode === 13 && element === 'messageTextBox') {
            $scope.sendMessage();
            $('#messageTextBox').val('');

        }
    };

    function scroll() {
        $timeout(function () {
            var objDiv = document.getElementById("parentMessages");
            objDiv.scrollTop = objDiv.scrollHeight;

        }, 100);
    }


}]);

chat.controller('change_password', ['$scope', '$http', '$cookies', function ($scope, $http, $cookies) {
    $scope.authenticated = false;
    $scope.passwordChanged = false;

    $scope.authenticate = function () {
        var r = {
            method: 'POST',
            url: '/login',
            headers: {
                'content-type': 'application/json'
            },
            data: {
                login: $scope.login,
                password: $scope.password
            }
        };
        $http(r).then(function s(resp) {
            if (resp.data.result === 'success') {
                var expires = new Date();
                expires.setSeconds(expires.getSeconds() + 60 * 60 * 24 * 14);
                $cookies.put('id', resp.data.id, {expires: expires});
                $cookies.put('auth', resp.data.token, {expires: expires});
                $cookies.put('username', resp.data.name, {expires: expires});
                $cookies.put('fayeToken', resp.data.faye_token, {expires: expires});
                $scope.adminId = resp.data.id;
                $scope.auth = resp.data.token;
                $scope.authenticated = true;
            } else {
                alert('error');
            }

        }, function f(resp) {
            console.log(resp.data);
        });
    };

    $scope.changePassword = function () {
        if ($scope.newPassword != $scope.newPasswordConfirmation) {
            alert('Different passwords');
            return;
        }
        var request = {
            method: 'POST',
            url: '/changepassword',
            headers: {
                'user-id': $scope.adminId,
                'auth-token': $scope.auth,
                'content-type': 'application/json'
            },
            data: {
                'password': $scope.newPassword,
                'password_confirmation': $scope.newPasswordConfirmation
            }
        };
        $http(request).then(function success(response) {
            console.log(response);
            if (response.data.result === 'success') {
                $scope.passwordChanged = true;
                $cookies.remove('id');
                $cookies.remove('auth');
                $cookies.remove('username');
                $cookies.remove('fayeToken');
            }
        }, function failure(response){

        });
    }
}]);